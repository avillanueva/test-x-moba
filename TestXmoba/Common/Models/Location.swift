//
//  Location.swift
//  TestXmoba
//
//  Created by Albert Villanueva on 01/03/2019.
//  Copyright © 2019 GeomotionGames. All rights reserved.
//

import CoreLocation

struct Location: Equatable {
  
  let street: String?
  let city: String?
  let coordinates: CLLocationCoordinate2D?
  
  static func == (lhs: Location, rhs: Location) -> Bool {
    return
      lhs.street == rhs.street &&
      lhs.city == rhs.city
  }
}

extension Dictionary {
  
  func toLocation() -> Location? {
    guard let dictionary = self as? [String : Any] else { return nil }
    
    var coordinates:CLLocationCoordinate2D? = nil
    
    if let coordinatesObject = dictionary["coordinates"] as? [String : Any] {
      if let longitude = coordinatesObject["longitude"] as? String, let latitude = coordinatesObject["latitude"] as? String {
        coordinates = CLLocationCoordinate2D(latitude: Double(latitude) ?? 0, longitude: Double(longitude) ?? 0)
      }
    }
    
    return Location(street: dictionary["street"] as? String, city: dictionary["city"] as? String, coordinates: coordinates)
  }
}
