//
//  User.swift
//  TestXmoba
//
//  Created by Albert Villanueva on 01/03/2019.
//  Copyright © 2019 GeomotionGames. All rights reserved.
//

import Foundation

struct User: Equatable {
  
  let name: String?
  let gender: String?
  let email: String?
  let location: Location?
  let imageUrl: String?
  
  static func == (lhs: User, rhs: User) -> Bool {
    return
      lhs.name == rhs.name &&
      lhs.gender == rhs.gender &&
      lhs.email == rhs.email &&
      lhs.location == rhs.location &&
      lhs.imageUrl == rhs.imageUrl
  }
}

extension Dictionary {
  
  func toUser() -> User {
    
    guard let dictionary = self as? [String : Any] else {
      return User(name: nil, gender: nil, email: nil, location: nil, imageUrl: nil)
    }
    
    var name:String? = ""
    var gender:String? = ""
    var location:Location?
    var imageUrl:String?
    
    if let nameObject = dictionary["name"] as? [String : Any] {
      if let firstName = nameObject["first"] as? String, let surname = nameObject["last"] as? String {
        name = "\(firstName) \(surname)"
      }
    }
    
    if let userGender = dictionary["gender"] as? String {
      gender = userGender
    }
    
    if let locationObject = dictionary["location"] as? [String : Any] {
      location = locationObject.toLocation()
    }
  
    if let pictureObject = dictionary["picture"] as? [String : Any] {
      if let pictureUrl = pictureObject["medium"] as? String {
        imageUrl = pictureUrl
      }
    }
    
    return User(name: name, gender: gender, email: dictionary["email"] as? String, location: location, imageUrl: imageUrl)
  }
}
