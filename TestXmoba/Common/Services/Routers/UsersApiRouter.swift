//
//  UsersApiRouter.swift
//  TestXmoba
//
//  Created by Albert Villanueva on 25/03/2019.
//  Copyright © 2019 GeomotionGames. All rights reserved.
//
import Alamofire

enum UsersApiRouter: URLRequestConvertible {
  case getUsers(results: Int, seed: String)
  
  static let baseURLString = "https://api.randomuser.me"
  
  var method: HTTPMethod {
    switch self {
    case .getUsers:
      return .get
    }
  }
  
  var path: String {
    switch self {
    case .getUsers:
      return "/"
    }
  }
  
  // MARK: URLRequestConvertible
  func asURLRequest() throws -> URLRequest {
    let url = try UsersApiRouter.baseURLString.asURL()
    
    var urlRequest = URLRequest(url: url.appendingPathComponent(path))
    urlRequest.httpMethod = method.rawValue
    
    switch self {
    case .getUsers(results: let numResults, seed: let seed):
      urlRequest = try URLEncoding.default.encode(urlRequest, with: ["results" : numResults, "seed": seed])
    }
    
    return urlRequest
  }
}
