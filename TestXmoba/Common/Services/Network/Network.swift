//
//  Network.swift
//  TestXmoba
//
//  Created by Albert Villanueva on 25/03/2019.
//  Copyright © 2019 GeomotionGames. All rights reserved.
//

import Alamofire

final class Network {
  
  static let sessionManager: SessionManager = {
    
    let configuration = URLSessionConfiguration.default
    configuration.httpAdditionalHeaders = SessionManager.defaultHTTPHeaders
    
    let manager = SessionManager(configuration: configuration)
    return manager
  }()
}
