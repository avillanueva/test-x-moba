//
//  UserApiStore.swift
//  TestXmoba
//
//  Created by Albert Villanueva on 01/03/2019.
//  Copyright © 2019 GeomotionGames. All rights reserved.
//

import Foundation
import Alamofire

class UserApiStore: UsersStoreProtocol {
  
  func getUsers(completion: @escaping (UsersStoreResult<[User]>) -> Void) {
    
    let request = UsersApiRouter.getUsers(results: 100, seed: "xmoba")
    Network.sessionManager.request(request).validate().responseJSON { (response) in
      
      switch response.result {
        
      case .success(let responseData):
        
        guard let responseArray = responseData as? [String : Any], let results = responseArray["results"] as? Array<[String : Any]> else {
          completion(UsersStoreResult.Failure(error: UsersStoreError.UnknownError()))
          return
        }
        
        let users = results.map({ $0.toUser() })
        completion(UsersStoreResult.Success(result: users))
        
      case .failure(let error):
        
        if let error = error as? URLError {
          
          switch error.code {
          case .notConnectedToInternet:
            completion(UsersStoreResult.Failure(error: UsersStoreError.NetworkError()))
          case .timedOut:
            completion(UsersStoreResult.Failure(error: UsersStoreError.TimedOut()))
          default:
            completion(UsersStoreResult.Failure(error: UsersStoreError.UnknownError()))
          }
        }
      }
    }
  }
}
