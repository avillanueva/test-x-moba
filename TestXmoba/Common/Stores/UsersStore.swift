//
//  UsersStore.swift
//  TestXmoba
//
//  Created by Albert Villanueva on 01/03/2019.
//  Copyright © 2019 GeomotionGames. All rights reserved.
//

import Foundation

protocol UsersStoreProtocol {
  
  func getUsers(completion: @escaping (UsersStoreResult<[User]>) -> Void)
}

enum UsersStoreResult<U> {
  
  case Success(result: U)
  case Failure(error: UsersStoreError)
}

enum UsersStoreError: Error, Equatable {
  
  case UnknownError()
  case NetworkError()
  case TimedOut()
}

