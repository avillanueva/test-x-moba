//
//  UsersMemStore.swift
//  TestXmoba
//
//  Created by Albert Villanueva on 06/03/2019.
//  Copyright © 2019 GeomotionGames. All rights reserved.
//

import Foundation

class UsersMemStore: UsersStoreProtocol {
  
  static var users = [User(name: "john", gender: "male", email: "john.hjk@gmail.com", location: nil, imageUrl: "https://randomuser.me/api/portraits/thumb/men/8.jpg"),
                      User(name: "hanna", gender: "female", email: "hanna.hjk@gmail.com", location: nil, imageUrl: "https://randomuser.me/api/portraits/thumb/women/58.jpg")]
  
  func getUsers(completion: @escaping (UsersStoreResult<[User]>) -> Void) {
    
    completion(UsersStoreResult.Success(result: UsersMemStore.users))
  }
}
