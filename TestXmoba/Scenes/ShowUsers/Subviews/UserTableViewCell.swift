//
//  UserTableViewCell.swift
//  TestXmoba
//
//  Created by Albert Villanueva on 04/03/2019.
//  Copyright © 2019 GeomotionGames. All rights reserved.
//

import UIKit
import Kingfisher

class UserTableViewCell: UITableViewCell {

  @IBOutlet weak var userImageView: UIImageView!
  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var emailLabel: UILabel!
  
  var user: User? {
    didSet {
      setupViews()
    }
  }

  func setupViews() {
    
    guard let user = self.user else { return }
    if let userName = user.name {
      nameLabel.text = userName
    }
    if let email = user.email {
      emailLabel.text = email
    }
    if let imageUrl = user.imageUrl {
      guard let url = URL(string: imageUrl) else { return }
      userImageView.kf.setImage(with: url)
    }
  }
}
