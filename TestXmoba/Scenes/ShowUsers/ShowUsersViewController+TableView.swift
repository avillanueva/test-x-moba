//
//  ShowUsersViewController+TableView.swift
//  TestXmoba
//
//  Created by Albert Villanueva on 04/03/2019.
//  Copyright © 2019 GeomotionGames. All rights reserved.
//

import UIKit

extension ShowUsersViewController: UITableViewDataSource, UITableViewDelegate {
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return users.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: UserTableViewCell.self)) as? UserTableViewCell else { return UITableViewCell() }
    cell.user = users[indexPath.row]
    
    return cell
  }
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    
    performSegue(withIdentifier: Constants.Segues.showUserDetail, sender: nil)
  }
}
