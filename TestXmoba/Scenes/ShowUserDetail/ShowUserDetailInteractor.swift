//
//  ShowUserDetailInteractor.swift
//  TestXmoba
//
//  Created by Albert Villanueva on 06/03/2019.
//  Copyright (c) 2019 GeomotionGames. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

protocol ShowUserDetailBusinessLogic
{
  func loadData()
}

protocol ShowUserDetailDataStore
{
  var user: User? { get set }
}

class ShowUserDetailInteractor: ShowUserDetailBusinessLogic, ShowUserDetailDataStore
{
  var user: User?
  var presenter: ShowUserDetailPresentationLogic?
  
  func loadData() {
    
    guard let user = self.user else { return }
    presenter?.presentUserData(response: ShowUserDetail.UserData.Response(user: user))
  }
}
