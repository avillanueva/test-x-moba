//
//  ShowUserDetailRouter.swift
//  TestXmoba
//
//  Created by Albert Villanueva on 06/03/2019.
//  Copyright (c) 2019 GeomotionGames. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

import UIKit

@objc protocol ShowUserDetailRoutingLogic
{}

protocol ShowUserDetailDataPassing
{
  var dataStore: ShowUserDetailDataStore? { get }
}

class ShowUserDetailRouter: NSObject, ShowUserDetailRoutingLogic, ShowUserDetailDataPassing
{
  weak var viewController: ShowUserDetailViewController?
  var dataStore: ShowUserDetailDataStore?
}
