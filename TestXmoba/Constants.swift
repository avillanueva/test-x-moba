//
//  Constants.swift
//  TestXmoba
//
//  Created by Albert Villanueva on 06/03/2019.
//  Copyright © 2019 GeomotionGames. All rights reserved.
//

import Foundation

struct Constants {
  
  struct Segues {
    static let showUserDetail = "ShowUserDetail"
  }
}
