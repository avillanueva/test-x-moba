//
//  UsersStoreTests.swift
//  TestXmobaTests
//
//  Created by Albert Villanueva on 06/03/2019.
//  Copyright © 2019 GeomotionGames. All rights reserved.
//

import XCTest
@testable import TestXmoba
class UsersStoreTests: XCTestCase {
  
  var sut: UsersMemStore!
  var testUsers: [User]!
  
  override func setUp() {
    super.setUp()
    setupUsersMemStore()
  }

  override func tearDown() {
    resetUsersMemStore()
    super.tearDown()
  }
  
  func setupUsersMemStore() {
    sut = UsersMemStore()
    testUsers = [Seeds.Users.testUser1, Seeds.Users.testUser2]
    UsersMemStore.users = testUsers
  }
  
  func resetUsersMemStore() {
    UsersMemStore.users = []
    sut = nil
  }

  func testGetUsersShouldReturnUsersList() {
    
    //When
    var fetchedUsers: [User] = []
    var usersStoreError: UsersStoreError?
    let expect = expectation(description: "wait for users to return")
    sut.getUsers { (result) in
      
      switch result {
      case .Success(result: let users):
        fetchedUsers = users
      case .Failure(error: let error):
        usersStoreError = error
      }
      expect.fulfill()
    }
    waitForExpectations(timeout: 1.0)
    
    //Then
    XCTAssertEqual(fetchedUsers.count, testUsers.count)
    for user in fetchedUsers {
      XCTAssert(testUsers.contains(where: { (testUser) -> Bool in
        return testUser == user
      }))
    }
    XCTAssertNil(usersStoreError)
  }

}
