//
//  ShowUserDetailInteractorTests.swift
//  TestXmoba
//
//  Created by Albert Villanueva on 06/03/2019.
//  Copyright (c) 2019 GeomotionGames. All rights reserved.
//
//  This file was generated by the Clean Swift Xcode Templates so
//  you can apply clean architecture to your iOS and Mac projects,
//  see http://clean-swift.com
//

@testable import TestXmoba
import XCTest

class ShowUserDetailInteractorTests: XCTestCase
{
  // MARK: Subject under test
  
  var sut: ShowUserDetailInteractor!
  
  // MARK: Test lifecycle
  
  override func setUp()
  {
    super.setUp()
    setupShowUserDetailInteractor()
  }
  
  override func tearDown()
  {
    super.tearDown()
  }
  
  // MARK: Test setup
  
  func setupShowUserDetailInteractor()
  {
    sut = ShowUserDetailInteractor()
  }
  
  // MARK: Test doubles
  
  class ShowUserDetailPresenterSpy: ShowUserDetailPresentationLogic
  {
    var presentUserDataCalled = false
    
    func presentUserData(response: ShowUserDetail.UserData.Response) {
      presentUserDataCalled = true
    }
  }
  
  // MARK: Tests
  
  func testShouldCallPresenterWhenLoadDataCalled()
  {
    //Given
    let presenterSpy = ShowUserDetailPresenterSpy()
    sut.presenter = presenterSpy
    sut.user = Seeds.Users.testUser1

    // When
    sut.loadData()
    
    // Then
    XCTAssertTrue(presenterSpy.presentUserDataCalled)
  }
}
