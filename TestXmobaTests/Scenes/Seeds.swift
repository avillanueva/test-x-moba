//
//  Seeds.swift
//  TestXmobaTests
//
//  Created by Albert Villanueva on 01/03/2019.
//  Copyright © 2019 GeomotionGames. All rights reserved.
//

import Foundation
import CoreLocation
@testable import TestXmoba
struct Seeds {
  
  struct Users {
    
    static let testUser1 = User(name: "john", gender: "male", email: "john.hjk@gmail.com", location: Locations.testLocation, imageUrl: "https://randomuser.me/api/portraits/thumb/men/8.jpg")
    static let testUser2 = User(name: "hanna", gender: "female", email: "hanna.hjk@gmail.com", location: Locations.testLocation, imageUrl: "https://randomuser.me/api/portraits/thumb/women/58.jpg")
  }
  
  struct Locations {
    
    static let testLocation = Location(street: "Bisbe Soler", city: "Caldes de Montbui", coordinates: CLLocationCoordinate2D(latitude: 41.538587, longitude: 2.2159141))
  }
}
